import * as pulumi from "@pulumi/pulumi";
import * as digitalocean from "@pulumi/digitalocean";
import * as kubernetes from '@pulumi/kubernetes';

const k8sVersions = digitalocean.getKubernetesVersions({})

const clusterName = "k8s-iac2";
const cluster = new digitalocean.KubernetesCluster(clusterName, {
  region: digitalocean.Region.FRA1,
  version: k8sVersions.then(x => x.latestVersion),
  nodePool: {
    name: clusterName + '-pool',
    size: digitalocean.DropletSlug.DropletS2VCPU2GB,
    nodeCount: 1
  }
});

export const kubeconfig = cluster.kubeConfigs[0].rawConfig;
const kubeProvider = new kubernetes.Provider("do-k8s", {
  kubeconfig: kubeconfig
});

const argoNs = new kubernetes.core.v1.Namespace("argocd-ns", {
  metadata: {
    name: "argocd"
  }
}, {
  provider: kubeProvider
})

const argocd = new kubernetes.yaml.ConfigFile("argocd-yaml", {
  file: "argocd.yaml",
  transformations: [
    (o) => {
      if (o) {
        if (o.metadata) {
          o.metadata.namespace = argoNs.metadata.name
        } else {
          o.metadata = {
            namespace: argoNs.metadata.name
          }
        }
      }
    }
  ]
}, {
  provider: kubeProvider,
  dependsOn: argoNs
})
